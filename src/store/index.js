import foodsReduces from "./modules/takeaway"
import { configureStore } from "@reduxjs/toolkit"

const store = configureStore({
    reducer: {
        foods: foodsReduces
    }
})

export default store