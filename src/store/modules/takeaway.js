import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";
const foodsStore = createSlice({
    name: "foods",
    initialState: {
        // 商品列表
        foodsList: [],
        // 菜单激活下标值
        activeIndex: 0,
        // 购物车
        cartList: [],


    },
    reducers: {
        // 获取商品列表
        setFoodsList(state, action) {
            state.foodsList = action.payload
        },
        // 更改activeIndex
        changeActiveIndex(state, action) {
            state.activeIndex = action.payload
        },
        // 添加购物车
        addCart(state, action) {
            // 初始化第一次点击的数量,原本是undefined
            action.payload.count = 1

            // 获取当前商品的count,id,判断食物是否已存在,如果存在,则新增数量,如果没有,则添加
            const item = state.cartList.find(item => item.id === action.payload.id)
            if (item) {
                item.count++
            } else {
                state.cartList.push(action.payload)
            }
        },
        // count增加
        addCount(state, action) {
            // 获取当前商品的count,id
            const item = state.cartList.find(item => item.id === action.payload.id)
            item.count++
        },
        // count减少
        subCount(state, action) {
            // 获取当前商品的count,id
            const item = state.cartList.find(item => item.id === action.payload.id)
            if (item.count === 0) {
                return
            } else {
                item.count--

            }
        },
        // 清除购物车
        clearCart(state) {
            state.cartList = []
        },
    }
})

// 异步获取数据
const { setFoodsList, changeActiveIndex, addCart, addCount, subCount,clearCart } = foodsStore.actions
const fetchFoodsList = () => {
    return async (dispatch) => {
        const res = await axios.get("http://localhost:3004/takeaway")
        dispatch(setFoodsList(res.data))
    }
}

export { fetchFoodsList, changeActiveIndex, addCart, addCount, subCount,clearCart }

export default foodsStore.reducer

