import NavBar from './components/NavBar'
import Menu from './components/Menu'
import Cart from './components/Cart'
import FoodsCategory from './components/FoodsCategory'
import './App.scss'


import { fetchFoodsList } from './store/modules/takeaway'
import { useEffect, useState } from 'react'
import { useSelector, useDispatch } from "react-redux";

function App() {
  // 获取dispatch函数
  const dispatch = useDispatch()

  // 调用useEffect触发axios异步请求!
  useEffect(() => {
    dispatch(fetchFoodsList());
  }, [dispatch]);

  const { foodsList, activeIndex } = useSelector(state => state.foods)
  return (
    <div className="home">
      {/* 导航 */}
      <NavBar />

      {/* 内容 */}
      <div className="content-wrap">
        <div className="content">
          <Menu />

          <div className="list-content">
            <div className="goods-list">
              {/* 外卖商品列表 */}
              {foodsList.map((item, index) => {
                return (
                  activeIndex === index && <FoodsCategory
                    key={item.tag}
                    // 列表标题
                    name={item.name}
                    // 列表商品
                    foods={item.foods}
                  />
                )
              })}
            </div>
          </div>
        </div>
      </div>

      {/* 购物车 */}
      <Cart />
    </div>
  )
}

export default App
